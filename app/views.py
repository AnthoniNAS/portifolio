from django import template
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404, redirect
from django.template import loader
from django.urls import reverse_lazy
from django.views.generic import ListView
from django.views.generic.edit import CreateView, DeleteView, UpdateView

from .models import Companheiro, Paciente, Funcionario
from .models import Atendimento, Test, Tratamento
from .forms import PacienteForm, CompanheiroForm


@login_required(login_url="/login/")
def index(request):

    context = {}
    context['segment'] = 'index'

    html_template = loader.get_template('layouts/index.html')
    return HttpResponse(html_template.render(context, request))


@login_required(login_url="/login/")
def pages(request):
    context = {}
    # All resource paths end in .html.
    # Pick out the html file name from the url. And load that template.
    try:

        load_template = request.path.split('/')[-1]
        context['segment'] = load_template

        html_template = loader.get_template(load_template)
        return HttpResponse(html_template.render(context, request))

    except template.TemplateDoesNotExist:

        html_template = loader.get_template('page-404.html')
        return HttpResponse(html_template.render(context, request))

    except:

        html_template = loader.get_template('page-500.html')
        return HttpResponse(html_template.render(context, request))


class PacienteListar(ListView):
    model = Paciente
    template_name = 'pacientes/lista.html'
    queryset = Paciente.objects.all()
    context_object_name = 'form'


def PacienteCreate(request, **kwargs):
    template = 'pacientes/create.html'
    form_paciente = PacienteForm(request.POST or None)
    form_companheiro = CompanheiroForm(request.POST or None)

    if request.method == 'POST':
        if form_paciente.is_valid():
            paciente = form_paciente.save()
            companheiro_dict = dict(
                acompanhante=form_companheiro.data.get('acompanhante'),
                apelido=form_companheiro.data.get('apelido'),
                dat_nsc=form_companheiro.data.get('dat_nsc'),
                Sexo=form_companheiro.data.get('Sexo'),
                Cpf=form_companheiro.data.get('Cpf'),
                nume_card_sus=form_companheiro.data.get('nume_card_sus'),
                paciente=paciente,
            )
            Companheiro.objects.create(**companheiro_dict)
            return redirect(reverse_lazy('pacientes'))

    context = {
        'form_paciente': form_paciente,
        'form_companheiro': form_companheiro
    }

    return render(request, template, context)


def Paciente_edit(request, pk):
    template = 'pacientes/create.html'
    obj = get_object_or_404(Paciente,  pk=pk)
    obj2 = get_object_or_404(Companheiro,  pk=pk)

    form_paciente = PacienteForm(request.POST or None, instance=obj)
    form_companheiro = CompanheiroForm(request.POST or None, instance=obj2)

    if form_paciente.is_valid():
        form_paciente.save()
        form_companheiro.save()

        return redirect(reverse_lazy('pacientes'))


    context = {
        'form_paciente': form_paciente,
        'form_companheiro': form_companheiro
    }

    return render(request, template, context)
