from django.db import models
from django.core.validators import RegexValidator



class Paciente(models.Model):
    SEXO = (
        ("F", "Feminino"),
        ("M", "Masculino")
    )

    nome = models.CharField(max_length=100, null=True, blank=True, verbose_name="Nome")
    apldo = models.CharField(max_length=50, verbose_name="Apelido")
    dt_nsc = models.DateField(max_length=15, verbose_name="Data de Nascimento")
    n_ident = models.CharField(max_length=15, verbose_name="RG")
    cpf = models.CharField(max_length=15, verbose_name="CPF", validators=[RegexValidator(regex='^.{11}$', message='Favor inserir o CPF.', code='nomatch')])
    num_card_sus = models.CharField(max_length=15, verbose_name="Cartão do SUS")
    sexo = models.CharField(max_length=10, choices=SEXO, blank=False, null=False)
    email = models.CharField(max_length=100)
    tel = models.CharField(max_length=100,verbose_name="Telefone")
    rua = models.CharField(max_length=100, null=True, blank=True, verbose_name="Rua")
    num = models.CharField(max_length=10, verbose_name="N°")
    bairro = models.CharField(max_length=50, verbose_name="Bairro")
    comple = models.CharField(max_length=50, verbose_name="Complemento")
    cep = models.CharField(max_length=15, verbose_name="CEP")
    cid = models.CharField(max_length=50, verbose_name="Cidade")



class Companheiro(models.Model):
    SEXO = (
        ("F", "Feminino"),
        ("M", "Masculino")
    )

    acompanhante = models.CharField(max_length=100)
    apelido = models.CharField(max_length=50)
    dat_nsc = models.DateField(max_length=15, null=True, blank=True)
    Sexo = models.CharField(max_length=10, choices=SEXO)
    Cpf = models.CharField(max_length=15)
    nume_card_sus = models.CharField(max_length=15)
    paciente = models.ForeignKey(Paciente,
        on_delete=models.CASCADE,
        related_name='companheiros',
        null=True,
        blank=True
    )


class Funcionario(models.Model):
    SEXO = (
        ("F", "F"),
        ("M", "M")
    )

    nome = models.CharField(max_length=100, null=True, blank=True)
    cpf = models.CharField(max_length=15)
    sexo = models.CharField(max_length=10, choices=SEXO, blank=False, null=False)
    cargo = models.CharField(max_length=100)

class Atendimento(models.Model):

    obs = models.TextField(max_length=200)
    id_parc = models.ForeignKey(Paciente, on_delete=models.CASCADE)
    id_func = models.ForeignKey(Funcionario, on_delete=models.CASCADE)

class Test(models.Model):

    tipo = models.CharField(max_length=50, null=True ,blank=True)
    procedimento = models.CharField(max_length=50, null=True ,blank = True)
    obs = models.TextField(max_length=200)
    id_func = models.ForeignKey(Funcionario, on_delete=models.CASCADE)
    id_atd = models.ForeignKey(Atendimento, on_delete=models.CASCADE)

class Tratamento(models.Model):

    tipo = models.CharField(max_length=50, null=True, blank=True)
    procedimento = models.CharField(max_length=50, null=True ,blank = True)
    obs = models.TextField(max_length=200)
    id_func = models.ForeignKey(Funcionario, on_delete=models.CASCADE)
    id_atd = models.ForeignKey(Atendimento, on_delete=models.CASCADE)
