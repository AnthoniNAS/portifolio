from django.urls import path, re_path
from app import views
from .views import PacienteListar, Paciente_edit

urlpatterns = [
    # Matches any html file - to be used for gentella
    # Avoid using your .html in your resources.
    # Or create a separate django app.
    re_path(r'^.*\.html', views.pages, name='pages'),

    # The home page
    path('', views.index, name='home'),
    path('pacientes', PacienteListar.as_view(), name='pacientes'),
    path('pacientes/create', views.PacienteCreate, name='pacientecreate'),
    path('pacientes/edit/<int:pk>', views.Paciente_edit, name='edit'),
]
