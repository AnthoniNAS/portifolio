from django.contrib import admin
from .models import Companheiro, Paciente


admin.site.register(Companheiro)
admin.site.register(Paciente)
