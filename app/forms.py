from django import forms
from validate_docbr import CPF
from django.forms.models import inlineformset_factory
from django.utils.translation import ugettext_lazy as _
from .models import Companheiro, Paciente, Funcionario, Atendimento, Test, Tratamento


class CompanheiroForm(forms.ModelForm):
    class Meta:
        model = Companheiro
        exclude = ('paciente',)

        widgets = {
            'dat_nsc': forms.DateInput(attrs={'type': 'date', format:'%d-%m-%Y'}),
        }

        labels = {
            'dat_nsc': _('Data de Nascimento'),
        }


class PacienteForm(forms.ModelForm):

    class Meta:
        model = Paciente
        fields = '__all__'

        widgets = {
            'dt_nsc': forms.DateInput(attrs={'type': 'date'}),
        }

        labels = {
            'dt_nsc': _('Data de Nascimento'),
        }

    # def clean_cpf(self):
    #    cpf = self.cleaned_data.get('cpf')

    #    validate_cpf = CPF()
    #    if not validate_cpf.validate(cpf):
    #        raise forms.ValidationError('O CPF é invalido')

    #    return cpf

#PacienteFormset = inlineformset_factory(companheirxs, pacientes, form=CompanheirxsForm, extra=1, can_delete=False, validate_min=True)


class FuncionarioModelForm(forms.ModelForm):
    class Meta:
        model = Funcionario
        fields = '__all__'


class AtendimentoModelForm(forms.ModelForm):
    class Meta:
        model = Atendimento
        fields = '__all__'


class TestModelForm(forms.ModelForm):
    class Meta:
        model = Test
        fields = '__all__'


class TratamentoModelForm(forms.ModelForm):
    class Meta:
        model = Tratamento
        fields = '__all__'
